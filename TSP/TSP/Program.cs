﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Diagnostics;
using System.IO;

// TODO - Move console.writeline statements to relevant methods in libloader.cs

namespace TSP
{
    class Program
    {
        static void Main(string[] args)
        {
            // Dataset to work on
            string dataSet = "ali535";
            // For tracking performance
            Stopwatch clock = new Stopwatch();
            long timeSpent;
            // For writing performance times to file
            StreamWriter sw = new StreamWriter("..\\..\\Resources\\" + dataSet + ".csv", true);

            // Setup
            List<PointF> cities = new List<PointF>();
            LibLoader loader = new LibLoader();
            cities = loader.LoadLib("..\\..\\Resources\\" + dataSet + ".tsp");
            // Unsorted list length
            Console.WriteLine("Unsorted route length: " + loader.RouteLength(cities));

            // Start clock
            clock.Start();
            // Perform nearest neighbour on list
            List<PointF> basicNn = loader.NearestNeighbour(cities);
            // Stop clock
            clock.Stop();
            // Get time passed
            timeSpent = clock.ElapsedMilliseconds;
            // Write time to file
            sw.Write(timeSpent + ",");
            // Output length after nn & time alg took to run
            Console.WriteLine("Nearest neighbour route length: " + loader.RouteLength(basicNn) + "in " + timeSpent + "ms.");
            // Ensure alg worked correctly - comment out during test runs
            loader.SanityCheck(cities, basicNn);

            // Start clock
            clock.Start();
            // Find best SP with regards to route length
            List<PointF> bestSP = loader.SwapSP(cities);
            // Stop clock
            clock.Stop();
            // Get elapsed time
            timeSpent = clock.ElapsedMilliseconds;
            // Write time to file
            sw.Write(timeSpent + ",");
            // Output new route length & time spent
            Console.WriteLine("Best SP route length: " + loader.RouteLength(bestSP) + "in " + timeSpent + "ms.");
            // Ensure alg worked correctly - comment out during test runs
            loader.SanityCheck(cities, bestSP);

            // Start clock
            clock.Start();
            // Optimise list
            List<PointF> optCities = loader.TwoOpt(bestSP);
            // Stop clock
            clock.Stop();
            // Get elapsed time
            timeSpent = clock.ElapsedMilliseconds;
            // Write time to file
            sw.Write(timeSpent + ",");
            // Output length after 2 opt & time spent
            Console.WriteLine("2 Opt applied to best sp nn route length: " + loader.RouteLength(optCities) + "in " + timeSpent + "ms.");
            // Ensure alg worked correctly - comment out during test runs
            loader.SanityCheck(cities, optCities);

            // Go onto new line in file
            sw.WriteLine();         
            // Close connection to file
            sw.Close();
            sw.Dispose();
            // Keep console open
            Console.ReadLine();
        }
    }
}
