﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;
using System.Text.RegularExpressions;

namespace pointsGraph
{
    class LibLoader
    {
        // Load city data into list of points
        public List<PointF> LoadLib (String file, MainWindow win)
        {
            win.consoleLstBx.Items.Add("Reading library from: " + file);
            // Create empty list to hold data read in
            List<PointF> result = new List<PointF>();

            try
            {
                // Prep file for reading
               using (StreamReader sr = new StreamReader(file))
               {
                   // Assign line read from file
                   String line;
                   // Are we still reading the file?
                   bool reading = false;
                   // Number of cities
                   int cities = 0;


                   // While there are lines to be read
                   while ((line = sr.ReadLine()) != null)
                   {
                       // Check for end of file
                       if (line.Contains("EOF"))
                       {

                           reading = false;
                           // Exit if loaded cities doesn't equal city count
                           if (result.Count() != cities)
                           {
                               Console.WriteLine("Error loading cities");
                               Environment.Exit(-1);
                           }
                       }

                    

                       // Parses node data
                       if (reading)
                       {
                           // Get rid of leading spaces
                           line = line.TrimStart();
                           // Split each line read by the spaces
                           String[] tokens = Regex.Split(line, @"\s+").ToArray();
                           // Grab x & y co-ords from parsed line
                           float x = float.Parse(tokens[1].Trim());
                           float y = float.Parse(tokens[2].Trim());
                           // Create a point from co-ords data
                           PointF city = new PointF(x, y);
                           // Add this to the list
                           result.Add(city);
                       }

                       // Determine dimension
                       if (line.Contains("DIMENSION"))
                       {
                           // Separate that line to find number 
                           String[] tokens = line.Split(':');
                           cities = Int32.Parse(tokens[1].Trim());
                       }

                       // Determine when node data has been reached
                       if (line.Contains("NODE_COORD_SECTION"))
                       {
                           // City node data follows this line - allow parsing
                           reading = true;
                       }


                   }
               }



            } 
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
         


            return result;
        }

        // Returns cumulative distance between all cities 
        public double RouteLength (List<PointF> cities)
        {
            // Holds route length
            double result = 0;
            // Holds prev city point (starts at last city in list)
            PointF prev = cities.ElementAt(cities.Count - 1);

            foreach (PointF city in cities)
            {
                double length = PointDistance(city, prev);
                result += length;
                // Set current city to prev for next run
                prev = city;

            }

            return result;
        }

        // Takes in two points, finds distance between them
        public double PointDistance (PointF cur, PointF other)
        {
            // Make vec of b-a
            PointF diffPoint = new PointF(other.X - cur.X, other.Y - cur.Y);
            // Find magnitude of b-a
            double result = Math.Sqrt(diffPoint.X * diffPoint.X + diffPoint.Y * diffPoint.Y);
            
            return result;
        }

        // Finds nearest city from current; brute force
        public List<PointF> NearestNeighbour (List<PointF> cities)
        {
            // Make a deep copy of given list so as not to change given list cities
            List<PointF> citiesCopy = new List<PointF>(cities);
            // Holds ordered list of cities
            List<PointF> result = new List<PointF>();
            // Holds distance between cities
            double distance = 0;
            // Holds smallest distance between cities 
            double smallestDistance = double.PositiveInfinity;
            // Holds city points of smallest distance
            PointF nearestCity = new PointF();

            // Take first city in cities as starting point (SP)
            result.Add(citiesCopy[0]);
            // Remove it from the list so it's not checked again
            citiesCopy.Remove(citiesCopy[0]);

            // For as many cities are left in list
            for (int i = 0; i < cities.Count - 1; i++)
            {
                // Check distance from SP to all unvisited cities
                foreach (PointF city in citiesCopy)
                {
                    // Find distance between that city and SP
                    distance = PointDistance(result[i], city);
                    //Console.WriteLine("Distance from " + result[i] + " is " + distance);
                    // If distance is less than smallest found distance, update distance and track that city
                    if (distance < smallestDistance)
                    {
                        // Update smallest distance
                        smallestDistance = distance;
                        // Update nearest city 
                        nearestCity = city;
                    }
                }

                // Add nearest city to result list
                result.Add(nearestCity);
                // Remove nearest city from cities list to avoid rechecking
                citiesCopy.Remove(nearestCity);
                // Reset smallest distance 
                smallestDistance = double.PositiveInfinity;

            }

            // Add first city to end
            result.Add(cities[0]);
            return result;
        }

        // 2opt - Swaps pairs of points, checks length, keeps change if shorter
        public List<PointF> TwoOpt(List<PointF> cities)
        {
            // Make a deep copy of given list so as not to change given list cities
            List<PointF> citiesCopy = new List<PointF>(cities);
            
            // Current route length to be improved upon
            double routeLength = RouteLength(cities);
            
            // Swap to the left, ignoring first value (start point)
            for (int i = 1; i < citiesCopy.Count(); i++)
            {
                // Ignoring first point (sp), swap next two points
                SwapPoints(citiesCopy, i, i-1);

                // Check new route length
                if (RouteLength(citiesCopy) > routeLength)
                {
                    // If new route length is larger, revert swap
                    SwapPoints(citiesCopy, i, i-1);
                }
                
            }
            // Output new route length
            Console.WriteLine(RouteLength(citiesCopy));
        // CHANGE THIS LATER TO OUTPUT CITIESCOPY - set to list in main
            return citiesCopy;
        }

        // Swaps two points in a given list at a given index
        private List<PointF> SwapPoints(List<PointF> list, int right, int left)
        {
            // Holds point to be swapped
            PointF swap;
            // Set value in list at index to swap
            swap = list[right];
            // Make indexed value equal to previous value
            list[right] = list[left];
            // Set previous to swap value
            list[left] = swap;

            return list;
        }
        
        public List<PointF> BestSP (List<PointF> cities)
        {
            // 1. Read in unordered list, run NN, record length
            // 2. Choose a new starting point (swap values around so as not to lose any), run NN, record length
            // 3. Keep track of shortest length, keep that SP

            // Make a deep copy of given list so as not to change given list cities
            List<PointF> citiesCopy = new List<PointF>(cities);
            // Best match list
            List<PointF> bestMatch = new List<PointF>();
            // Run nearest neighbour, set to route length
            double bestRoute = RouteLength(NearestNeighbour(citiesCopy));
            Console.WriteLine("Unchanged list SP route length: " + bestRoute);
            // Check against each point as SP
            for (int i = 1; i < citiesCopy.Count; i++)
            {
                // Swap first value with next value
                SwapPoints(citiesCopy, i, 0);
                // Recheck route length of NN
                if (RouteLength(citiesCopy) < bestRoute)
                {
                    bestMatch = citiesCopy;
                }

            }
           

            return bestMatch;
        }
    }
}
