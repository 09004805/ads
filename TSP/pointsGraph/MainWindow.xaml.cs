﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Drawing;

namespace pointsGraph
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        // Initialise program class
        Program prog = new Program();
        // Used to scale lines on canvas
        float scalar = 1.5f;

        public MainWindow()
        {
            InitializeComponent();    
            prog.Run(this);
        }

        private void BtnNN_Click(object sender, RoutedEventArgs e)
        {
            
            // Get a list of cities sorted via NN only
            List<PointF> citiesNn = prog.NearestNeighbour(this);

            // Clear canvas from any previous graphs
            canvas.Children.Clear();

            // For every point
            for (int i = 0; i < citiesNn.Count ; i++)
            {
                // Create a new line
                Line line = new Line();

                line.Visibility = System.Windows.Visibility.Visible;
                line.StrokeThickness = 2;
                line.Stroke = System.Windows.Media.Brushes.Black;
                // Set first point values
                line.X1 = citiesNn[i].X * scalar;
                line.Y1 = citiesNn[i].Y * scalar;

                // If it's reached the final value
                if (i == citiesNn.Count - 1)
                {
                    // Set second points to first
                    line.X2 = citiesNn[0].X * scalar;
                    line.Y2 = citiesNn[0].Y * scalar;
                }
                else
                {
                    // Set second points to next value
                    line.X2 = citiesNn[i + 1].X * scalar;
                    line.Y2 = citiesNn[i + 1].Y * scalar;
                }

                canvas.Children.Add(line);
            }

        }

        private void optBtn_Click(object sender, RoutedEventArgs e)
        {
            // Get a list of cities from prog that have been optimised from nn
            List<PointF> optCities = prog.TwoOpt(this);

            // Clear canvas from any previous graphs
            canvas.Children.Clear();

            // For every point
            for (int i = 0; i < optCities.Count; i++)
            {
                // Create a new line
                Line line = new Line();

                line.Visibility = System.Windows.Visibility.Visible;
                line.StrokeThickness = 2;
                line.Stroke = System.Windows.Media.Brushes.Black;
                // Set first point values
                line.X1 = optCities[i].X * scalar;
                line.Y1 = optCities[i].Y * scalar;

                // If it's reached the final value
                if (i == optCities.Count - 1)
                {
                    // Set second points to first
                    line.X2 = optCities[0].X * scalar;
                    line.Y2 = optCities[0].Y * scalar;
                }
                else
                {
                    // Set second points to next value
                    line.X2 = optCities[i + 1].X * scalar;
                    line.Y2 = optCities[i + 1].Y * scalar;
                }

                canvas.Children.Add(line);
            }
        }

        private void btnNN_Click_1(object sender, RoutedEventArgs e)
        {

        }
    }
}
