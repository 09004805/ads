﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

// TODO - Move console.writeline statements to relevant methods in libloader.cs

namespace pointsGraph
{
    class Program
    {
        // Instantiate libloader 
        LibLoader loader = new LibLoader();
        // Lists of cities, various stages of being sorted
        List<PointF> unsortedCities;
        List<PointF> nnCities;
        List<PointF> twoOptCities;
        List<PointF> bestSP;

        // Loads in points library to a list to be worked on
        public void Run(MainWindow win)
        {
            // Setup
            unsortedCities = new List<PointF>();
            // Read points from file into list
            unsortedCities = loader.LoadLib("..\\..\\Resources\\a280.tsp", win);
            // Unsorted list length
            win.consoleLstBx.Items.Add("Unsorted route length: " + loader.RouteLength(unsortedCities));   
        }


        // Takes in list of cities to solve via NN method
        public List<PointF> NearestNeighbour(MainWindow win)
        {
            // Write to console list box
            win.consoleLstBx.Items.Add("Performing nearest neighbour algorithm...");
            // Run nearest neighbout alg on given list
            nnCities = loader.NearestNeighbour(unsortedCities);

            return nnCities;
        }

        // Takes NN sorted cities, applies 2 opt 
        public List<PointF> TwoOpt(MainWindow win)
        {
            // Report to console list box
            win.consoleLstBx.Items.Add("Performing 2 Opt on cities list...");
            // Run 2 opt alg on nn sorted cities list
            twoOptCities = loader.TwoOpt(nnCities);

            return twoOptCities;
        }

        public List<PointF> BestSP(MainWindow win)
        {
            // Report to console list box
            win.consoleLstBx.Items.Add("Finding best starting point...");
            // Run sp alg
            bestSP = loader.BestSP(nnCities);

            return bestSP;
        }
        
    }
}
